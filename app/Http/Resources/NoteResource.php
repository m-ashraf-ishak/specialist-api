<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NoteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this -> id,
            'ticket_id' => $this -> ticket_id,
            'date' => $this -> date,
            'title' => $this -> title,
            'sketch' => $this -> sketch,
            'summary' => $this -> summary,
            'evidence' => $this -> evidence,
            'timetable' => $this -> timetable,
            'duration' => $this -> duration
        ];
    }
}
