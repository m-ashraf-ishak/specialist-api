<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TicketResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this -> id,
            'number' => $this -> number,
            'subject' => $this -> subject,
            'severity' => $this -> severity,
            'product' => $this -> product,
            'problem_statement' => $this -> problem_statement,
            'actual_problem' => $this -> actual_problem,
            'expected_result' => $this -> expected_result,
            'business_impact' => $this -> business_impact,
            'jira' => $this -> jira,
            'sme' => $this -> sme,
            'customer_name' => $this -> customer_name,
            'customer_email' => $this -> customer_email,
            'customer_phone' => $this -> customer_phone,
            'resolution' => $this -> resolution,
            'notes' => NoteResource::collection($this -> notes() -> orderBy('date') -> get())
        ];
    }
}
