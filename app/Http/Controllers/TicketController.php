<?php

namespace App\Http\Controllers;

use App\Http\Resources\TicketResource;
use App\Note;
use App\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response() -> json(TicketResource::collection(Ticket::all()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ticket = null;
        Log::debug('Create request', ['$request -> json() -> all()' => $request -> json() -> all()]);
        if ($request -> isJson()){
            $ticket = Ticket::create($request -> json() -> all());
        } else {
            $ticket = Ticket::create($request -> all());
        }
        $ticket -> save();
        return $this -> index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ticket = Ticket::find($id);
        if ($request -> isJson()){
            // NOTE: Workaround for updating child nodes. Not sure if there is any cleaner way
            if (sizeof($request -> json('notes')) > 0){
                $noteJsons = $request -> json('notes');
                Log::debug('notes json', ['$noteJsons' => $noteJsons]);
                foreach ($noteJsons as $noteJson){
                    $note = null;
                    if (!isset($noteJson['id'])|| Note::find($noteJson['id']) == null){
                        // Hack it punchy
                        if (isset($noteJson['date'])){
                            $dateTime = new \DateTime($noteJson['date']);
                            $noteJson['date'] = $dateTime -> format('y-m-d');
                        }
                        $note = $ticket -> notes() -> create($noteJson);
                    } else {
                        $note = Note::find($noteJson['id']);
                        $note -> update($noteJson);
                    }
                    $note -> save();
                }
                $request -> json() -> remove('notes');
            }
            $ticket -> update($request -> json() -> all());
        } else {
            $ticket -> update($request -> all());
        }
        $ticket -> save();
        return $this -> index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
