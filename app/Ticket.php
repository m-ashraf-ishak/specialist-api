<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    //

    protected $guarded = ['id'];

    public function notes(){
        return $this -> hasMany(Note::class);
    }

    public function links(){
        return $this -> hasMany(Link::class);
    }

    public function webexs(){
        return $this -> hasMany(Webex::class);
    }
}
