<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table -> string('number');
            $table -> string('subject') -> nullable();
            $table -> string('severity') -> nullable();
            $table -> string('product') -> nullable();
            $table -> text('problem_statement') -> nullable();
            $table -> text('actual_problem') -> nullable();
            $table -> text('expected_result') -> nullable();
            $table -> text('business_impact') -> nullable();
            $table -> string('jira') -> nullable();
            $table -> string('sme') -> nullable();
            $table -> string('customer_name') -> nullable();
            $table -> string('customer_email') -> nullable();
            $table -> string('customer_phone') -> nullable();
            $table ->  text('resolution') -> nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
