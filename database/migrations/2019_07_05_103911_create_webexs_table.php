<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebexsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webexs', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table -> integer('ticket_id') -> index();
            $table -> date('date');
            $table -> string('attendees') -> nullable();
            $table -> text('actions') -> nullable();
            $table -> text('next_actions') -> nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webexs');
    }
}
